package com.jk.demo1;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Service {
    public BuildBodyVo buildParameter(Class<?> tClass) {
        List<ParameterVo> params = new ArrayList<>();
        StringBuilder simpleBuilder = new StringBuilder();
        parseParams(tClass, params, simpleBuilder, 0);
        return BuildBodyVo.builder().parameterVoList(params).simpleBuilder(simpleBuilder).build();
    }

    private void parseParams(Class<?> tClass, List<ParameterVo> parameters, StringBuilder simpleBuilder, int deep) {
        // 计算制表符
        StringBuilder tableSpace = new StringBuilder();
        for (int i = 0; i < deep; i++) {
            tableSpace.append(" ");
        }

        // 构建开局符号
        simpleBuilder.append("{").append(System.lineSeparator());
        String tableSpaceField = tableSpace + "  ";

        Field[] declaredFields = tClass.getDeclaredFields();
        for (int i = 0; i < declaredFields.length; i++) {
            Field field = declaredFields[i];
            String name = field.getName();
            Object defaultValue = null;
            if (TypeUtils.INTEGER_TYPE.contains(field.getType().getTypeName())) {
                // 处理整数类型
                defaultValue = 0;
            } else if (TypeUtils.STRING_TYPE_CLASS.equals(field.getType().getTypeName())) {
                // 处理字符串类型
                defaultValue = "String";
            } else if (TypeUtils.TIME_TYPE.contains(field.getType().getTypeName())) {
                // 处理时间类型
                defaultValue = "\"2023-09-27 00:17:20\"";
            } else if (field.getType().isEnum()) {
                // 处理枚举类型
                defaultValue = "\"" + field.getType().getEnumConstants()[0] + "\"";
            } else if (TypeUtils.DECIMAL_TYPE.contains(field.getType().getTypeName())) {
                // 处理小数类型
                defaultValue = 0.0;
            } else if (TypeUtils.LIST_TYPE.contains(field.getType().getTypeName())) {
                // 处理列表类型
                simpleBuilder.append(tableSpaceField).append(name).append(": ").append("[").append(System.lineSeparator());
                Type genericType = field.getGenericType();
                ParameterizedType type = (ParameterizedType) genericType;
                Type actualTypeArgument = type.getActualTypeArguments()[0];
                simpleBuilder.append(tableSpaceField).append("  ").append("\"").append(actualTypeArgument.getTypeName()).append("\"").append(System.lineSeparator());
                simpleBuilder.append(tableSpaceField).append("]");
                if (i == declaredFields.length - 1) {
                    simpleBuilder.append(System.lineSeparator());
                } else {
                    simpleBuilder.append(",").append(System.lineSeparator());
                }
                continue;
            } else {
                // 处理bean对象类型
                simpleBuilder.append(tableSpaceField).append(name).append(": ");
                parseParams(field.getType(), parameters, simpleBuilder, deep + 2);
                if (i == declaredFields.length - 1) {
                    simpleBuilder.append(System.lineSeparator());
                } else {
                    simpleBuilder.append(",").append(System.lineSeparator());
                }
                continue;
            }
            parameters.add(ParameterVo.builder().name(name).defaultValue(defaultValue).description("整数").require(true).build());
            simpleBuilder.append(tableSpaceField).append(name).append(": ").append(defaultValue).append(i == declaredFields.length - 1 ? "" : ",").append(System.lineSeparator());
        }
        simpleBuilder.append(tableSpace).append("}");
    }
}
