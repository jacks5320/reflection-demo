package com.jk.demo1;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Teacher {
    /**
     * 教学学科
     */
    private String subject;
}
