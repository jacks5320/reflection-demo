package com.jk.demo1;


import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Setter
@Getter
public class Student extends Person {
    /**
     * 学科老是
     */
    private List<Teacher> teachers;

    /**
     * 成绩集合
     */
    private Map<String, Double> scores;

    /**
     * 一卡通
     */
    private Card card;
}
