package com.jk.demo1;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class BuildBodyVo {
    private List<ParameterVo> parameterVoList;

    private StringBuilder simpleBuilder;
}
