package com.jk.demo1;

import java.util.ArrayList;
import java.util.List;

public class TypeUtils {
    public static final List<String> BASE_TYPE_CLASS = new ArrayList<String>() {{
        add("java.lang.String");
        add("byte");
        add("java.lang.Byte");
        add("sort");
        add("java.lang.Short");
        add("int");
        add("java.lang.Integer");
        add("long");
        add("java.lang.Long");
        add("double");
        add("java.lang.Double");
        add("float");
        add("java.lang.Float");
        add("boolean");
        add("java.lang.Boolean");
        add("char");
        add("java.lang.Character");
    }};

    public static final List<String> INTEGER_TYPE = new ArrayList<String>() {
        {
            add("byte");
            add("java.lang.Byte");
            add("sort");
            add("java.lang.Short");
            add("int");
            add("java.lang.Integer");
            add("long");
            add("java.lang.Long");
        }
    };

    public static final List<String> DECIMAL_TYPE = new ArrayList<String>() {{
        add("double");
        add("java.lang.Double");
        add("float");
        add("java.lang.Float");
    }};

    public static final String STRING_TYPE_CLASS = "java.lang.String";

    public static final List<String> TIME_TYPE = new ArrayList<String>() {{
        add("java.util.Date");
        add("java.sql.Date");
        add("java.sql.Timestamp");
    }};

    public static final List<String> LIST_TYPE = new ArrayList<String>() {{
        add("java.util.List");
        add("java.util.ArrayList");
        add("java.util.Set");
    }};
}
