package com.jk.demo1;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class Person {
    /**
     * 身份证
     */
    private String id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 年龄
     */
    private int age;

    /**
     * 身高
     */
    private double height;

    /**
     * 体重
     */
    private float weight;

    /**
     * 性别
     */
    private GenderEnum sex;

    /**
     * 生日
     */
    private Date birthday;
}
