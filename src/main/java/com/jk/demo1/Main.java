package com.jk.demo1;

/**
 * 通过反射生成接口文档，主要包括 参数列表和示例代码
 */
public class Main {
    public static void main(String[] args) {
        Service personService = new Service();
        BuildBodyVo buildBodyVo = personService.buildParameter(Student.class);
        System.out.println(buildBodyVo.getParameterVoList());
        System.out.println(buildBodyVo.getSimpleBuilder());
    }
}
