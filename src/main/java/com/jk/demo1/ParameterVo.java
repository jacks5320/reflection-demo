package com.jk.demo1;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ParameterVo {
    private String name;

    private String description;

    private String type;

    private boolean require;

    private Object defaultValue;
}
