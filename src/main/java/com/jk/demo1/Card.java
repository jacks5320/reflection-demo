package com.jk.demo1;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Card {
    /**
     * 卡号
     */
    private String id;

    /**
     * 学号
     */
    private int number;

    /**
     * 姓名
     */
    private String name;

    /**
     * 密码
     */
    private String password;

    /**
     * 余额
     */
    private double balanceMoney;
}
